package org.tw.battle.dbTest;

import org.tw.battle.domain.ServiceConfiguration;
import org.tw.battle.infrastructure.DatabaseConfiguration;

public class TestDatabaseConfiguration {
    private static final DatabaseConfiguration configuration = new
        DatabaseConfiguration("jdbc:h2:mem:testDB;MODE=MYSQL;", "sa", "p@ssword", "org.h2.Driver");

    public static ServiceConfiguration getConfiguration() {
        return configuration;
    }
}
